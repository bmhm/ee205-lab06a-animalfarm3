///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file nunu.hpp
/// @version 1.0
///
/// Exports data about all fish
///
/// @author Brooke Maeda <bmhm@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   11 February 2021
///////////////////////////////////////////////////////////////////////////////
#pragma once

#include <string>

#include "fish.hpp"

using namespace std;


namespace animalfarm {

class Nunu : public Fish {
public:
	bool isNative;
	
	Nunu( bool newNative, enum Color newColor, enum Gender newGender );

	void printInfo();
};

} // namespace animalfarm

