///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file the.hpp
/// @version 1.0
///
/// test
///
/// @author Brooke Maeda <bmhm@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   4 March 2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>


#include "animal.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "fish.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "bird.hpp"
#include "palila.hpp"
#include "nene.hpp"


using namespace std;
using namespace animalfarm;

int main(){
   

   for(int i = 0; i<10; i++){
      //cout << Animal::getRandomGender() << endl;
      //cout << Animal::getRandomColor() << endl;
      //cout << Animal::getRandomBool() << endl;
     //cout << Animal::getRandomWeight(12.2, 30.5) << endl;
      //cout << Animal::getRandomName() << endl;
      Animal* a = AnimalFactory::getRandomAnimal();
      cout << a->speak() << endl;
   }
  // cout << "I am Sam" << endl;
   return 0;

}




