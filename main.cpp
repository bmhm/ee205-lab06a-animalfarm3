///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file main.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Brooke Maeda <bmhm@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   4 March 2021
///////////////////////////////////////////////////////////////////////////////
#include <iostream>

#include "animal.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "fish.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "bird.hpp"
#include "palila.hpp"
#include "nene.hpp"
#include <array>
#include <list>


using namespace std;
using namespace animalfarm;

int main(){
 cout << "Welcome to Animal Farm 3" << endl;

//array
 array<Animal*, 30> animalArray;
 animalArray.fill( NULL );
 for (int i = 0; i < 25; i++){
   animalArray[i] = AnimalFactory::getRandomAnimal();

 }

 cout << endl << "Array of Animals" << endl;
 cout << "  Is it empty: " << ( animalArray.empty() ? "true" : "false" ) << endl;
 cout << "  Number of elements: " << animalArray.size() << endl;
 cout << "  Max size: " << animalArray.max_size() << endl;

 for ( Animal* animal : animalArray){
   if(animal == NULL)
      break;
   cout << animal->speak() << endl;
   
 }

//cleanup
 for( int i = 0; i < 25; i++){
   delete animalArray[i];
 }


//list
 list<Animal*> animalList;
 for (int i = 0; i < 25; i++){
   animalList.push_front(AnimalFactory::getRandomAnimal());

 }


 cout << endl << "List of Animals" << endl;
 cout << "  Is it empty: " << ( animalList.empty() ? "true" : "false" ) << endl;
 cout << "  Number of elements: " << animalList.size() << endl;
 cout << "  Max size: " << animalList.max_size() << endl;

 for ( Animal* animal : animalList){
   cout << animal->speak() << endl;
 }

//cleanup
 while( !animalList.empty()) {
   delete animalList.front();
   animalList.pop_front();

 }

}//end of main


