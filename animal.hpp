///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file animal.hpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Brooke Maeda <bmhm@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   4 March 2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

using namespace std;

#include <string>

namespace animalfarm {

enum Gender { MALE, FEMALE, UNKNOWN };

enum Color { BLACK, WHITE, RED, SILVER, YELLOW, BROWN };  /// @todo Add more colors

class Animal {

   public: //constructors
   Animal();
   ~Animal();

public: //member variables
	enum Gender gender;
	string      species;

public: //member functions
	virtual const string speak() = 0;
	void printInfo();

public: //static	
	string colorName  (enum Color color);
	string genderName (enum Gender gender);

   static const Gender  getRandomGender();
   static const Color   getRandomColor();
   static const bool    getRandomBool();
   static const float   getRandomWeight(const float from, const float to);
   static const string  getRandomName();


};

class AnimalFactory{
public:
   static Animal* getRandomAnimal();
};


} // namespace animalfarm

